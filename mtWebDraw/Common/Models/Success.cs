﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mtWebDraw.Common
{
    /// <summary>
    /// ExtJS返回值类型
    /// </summary>
    public class Success
    {

        /// <summary>
        /// ExtJS返回值类型
        /// </summary>
        /// <param name="Code">返回代码，>=1表示成功</param>
        /// <param name="Data">返回数据</param>
        /// <param name="Message">返回消息</param>
        public Success(int Code, object Data, string Message)
        {
            this.success = true;
            this.code = Code;
            this.data = Data;
            this.msg = Message;
        }

        /// <summary>
        /// 是否成功返回：用于表单提交时代表返回成功，默认为true
        /// </summary>
        public bool success { get; set; }

        /// <summary>
        /// 返回代码，>=1表示成功
        /// </summary>
        public int code { get; set; }

        /// <summary>
        /// 返回数据
        /// </summary>
        public object data { get; set; }

        /// <summary>
        /// 返回消息
        /// </summary>
        public string msg { get; set; }

    }
}